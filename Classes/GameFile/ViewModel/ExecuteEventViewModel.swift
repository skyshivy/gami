//
//  ExecuteEvent.swift
//  ShakeAndWin
//
//  Created by SKY on 11/12/20.
//

import Foundation
class ExecuteEventViewModel {
    func executeEvent(complition:(()->Void)?) {
        let myDict: Dictionary = [
            "requestId": Common.getRandonNo(),
            "timestamp": Common.getTimeStamp(),
                "keyword": "executeEvent",
            "queryParams": [["key": StoreManager.shared.msisdn, "keyType": "Msisdn"],["key": StoreManager.shared.language, "keyType": "LANG"], ["key": "24", "keyType": "GAME"], ["key": "17", "keyType": "MILESTONE"], ["key": "AssignReward", "keyType": "Activity"]]
        ] as [String : Any]
        
        NetworkManager.post(myStruct: ExecuteEvent.self, url: Constant.stcExecuteEvent, request: myDict) { (data, error) in
            if let _ = data {
                
                complition?()
            } else {
                //print("Error found")
            }
//            print("data is ", data)
//            print("Error is ", error)
        }
    }
}

