//
//  PopupView.swift
//  Gamification
//
//  Created by SKY on 09/03/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit
enum PopupViewAction {
    case guessOk
}
class PopupView: UIView {
    @IBOutlet weak var guessImageView: UIImageView!
    @IBOutlet weak var guessTitleLabel: UILabel!
    @IBOutlet weak var guessSubTitleLabel: UILabel!
    @IBOutlet weak var guessOkButton: UIButton!
    var errorData: ErrorData?
    var handler:((PopupViewAction)->Void)?
    static func loadXib(nibName: String) -> PopupView {
        return UINib(nibName: nibName, bundle: Bundle(for: PopupView.self)).instantiate(withOwner: self, options: nil).first as! PopupView
    }
    
    @IBAction func guessOkButtonAction() {
        handler?(.guessOk)
    }
    
    func populateGuessAndWinData(info: ErrorData?) {
        guard let inf = info else { return }
        print("data is \(info)")
        setupImage(info: inf)
        guessSubTitleLabel.text = inf.message
    }
   
    func setupImage(info: ErrorData) {
        var finalUrl = ""
        if let imgDat = info.imageUrl {
            if imgDat.contains("http") {
                finalUrl = imgDat
            } else {
                finalUrl = Constant.imageUrl+"\(imgDat)"
            }
            guessImageView.downloadImage(url: finalUrl,placeHolder: UIImage(named: "Loading", in: Bundle(for: PopupView.self), compatibleWith: nil))
        } else {
            
        }
    }
    
    func guessSuccessData() {
        guessTitleLabel.text = "Correct Answer".localizeFor(Self.self)
        guessSubTitleLabel.text = "You have entered the draw".localizeFor(Self.self)
        guessImageView.image = UIImage(named: "guessWin", in: Bundle(for: PopupView.self), compatibleWith: nil)
    }
    func guessFailData() {
        guessTitleLabel.text = "Incorrect Answer".localizeFor(Self.self)
        guessSubTitleLabel.text = "Try again later".localizeFor(Self.self)
        guessImageView.image = UIImage(named: "GuessTryNext", in: Bundle(for: PopupView.self), compatibleWith: nil)
    }
    
}
