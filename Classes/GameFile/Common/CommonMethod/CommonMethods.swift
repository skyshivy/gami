//
//  CommonMethods.swift
//  Gamification
//
//  Created by SKY on 15/05/21.
//

import UIKit
class CommomMethod {
    
    
    static func convertStringIntoDate(date: String) -> Int{
        let isoDate = date
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from:isoDate) {
            let timeInterval = date.timeIntervalSinceNow
            let myInt = Int(timeInterval)
            return myInt
        } else {
            return 2
        }
    }
    
    static func secondsToHoursMinutesSeconds (seconds : Int) -> (String, String, String) {
        return (String(format: "%02d",seconds / 3600), String(format: "%02d", (seconds % 3600) / 60), String(format: "%02d", (seconds % 3600) % 60))
    }
}
