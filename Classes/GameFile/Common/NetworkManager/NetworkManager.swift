//
//  NetworkManager.swift
//  ShakeAndWin
//
//  Created by SKY on 10/12/20.
//

import UIKit

enum NetworkManagerError {
    case error
    case tokenExpired
}

class NetworkManager {
    
    static var timer: Timer?
    static func post<T: Decodable>(myStruct: T.Type, url: String, request: Dictionary<String, Any>, complition:((T?, NetworkManagerError?)->Void)?) {
        guard let url1 = URL(string: url) else {return}
        print("url is \(url)")
        print("Request is \(request)")
        var req = URLRequest(url: url1)
        let jsonData = try? JSONSerialization.data(withJSONObject: request, options:[])
        let apiKey = "fUr6AkApOSJXnplqTKfnof80WhtELjDD"
        
        req.httpMethod = "POST"
        req.httpBody = jsonData
        req.timeoutInterval = 30
        
        //req.addValue(apiKey, forHTTPHeaderField: "x-apiKey")//this is only for STC. Comment this for normal this
      
       // req.addValue(accessToken, forHTTPHeaderField: "security_token")
        dataTask(req: req, complition: complition)

    }
    
    static func postWithHeader<T: Decodable>(myStruct: T.Type, urlReq: URLRequest, request: Dictionary<String, Any>, complition:((T?, NetworkManagerError?)->Void)?) {
        var req: URLRequest = urlReq
        print("Json data is \(request)")
        let jsonData = try? JSONSerialization.data(withJSONObject: request, options:[])
        print("Json data is \(jsonData)")
        req.httpMethod = "POST"
        req.httpBody = jsonData
        req.timeoutInterval = 30
       // req.addValue(accessToken, forHTTPHeaderField: "security_token")
        dataTask(req: req, complition: complition)
    }
    
    static func getWithHeader<T: Decodable>(myStruct: T.Type, urlReq: URLRequest, complition:((T?, NetworkManagerError?)->Void)?) {
        var req: URLRequest = urlReq
        let accessToken = StoreManager.shared.token
        req.httpMethod = "GET"
        //req.httpBody = jsonData
        req.timeoutInterval = 30
        //req.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
       // req.addValue(accessToken, forHTTPHeaderField: "security_token")
        dataTask(req: req, complition: complition)
    }
    
    static func get<T: Decodable>(myStruct: T.Type, url: String, complition:((T?, NetworkManagerError?)->Void)?) {
        guard let url1 = URL(string: url) else { return }
        print("url is \(url)")
        var req = URLRequest(url: url1)
        req.httpMethod = "GET"
        req.timeoutInterval = 30
        //req.addValue(accessToken, forHTTPHeaderField: "security_token")
        dataTask(req: req, complition: complition)
    }
    
    static func dataTask<T: Decodable>(req: URLRequest,complition:((T?, NetworkManagerError?)->Void)?) {
        if !Reachability().isConnectedToNetwork() {
            print(" no network connection")
            showAlert(message: "Please check internet connection.")
            return
        }
        
        DispatchQueue.main.async {
            timer?.invalidate()
            timer = nil
        }
        var req1 = req
        let accessToken = StoreManager.shared.token
        req1.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        req1.addValue("application/json", forHTTPHeaderField: "Content-Type")
        print("req is \(req1.allHTTPHeaderFields)")
        let task = URLSession.shared.dataTask(with: req1) { (data, responce, error) in
            print("\n\n")
            print("url is == \(req1.url)")
            print("\n\n\n\n")
            if let data = data {
                print("data into string",String(data: data, encoding: .utf8))
                print("data in json", try? JSONSerialization.jsonObject(with: data, options: []))
                let jsonData = try? JSONSerialization.jsonObject(with: data, options: [])
                let  respCode = (jsonData as? NSDictionary)?.value(forKey: "respCode") as? String
                if respCode == "SC0403" {
                    DispatchQueue.main.async {
                        timer?.invalidate()
                        timer = nil
                        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                            if Game.isTokenUpdated {
                                print("token is updated = ")
                                Game.isTokenUpdated = false
                                dataTask(req: req, complition: complition)
                            }
                        }
                    }
                    complition?(nil,.tokenExpired)

                    return
                }
                DispatchQueue.main.async {
                    timer?.invalidate()
                    timer = nil
                }
                let list = try? JSONDecoder().decode(T.self, from: data)
                complition?(list, nil)
            } else {
                complition?(nil, .error)
            }
        }
        task.resume()
    }
    
    private static func showAlert(message : String) {
        DispatchQueue.main.async {
            
            let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title:  "Ok".localizeFor(NetworkManager.self) , style: .default) { action in
                print("You've pressed OK Button")
            }
            alertController.addAction(OKAction)
            let wind = UIApplication.shared.windows.last?.rootViewController
            wind?.present(alertController, animated: true, completion: nil)
        }
    }
}
