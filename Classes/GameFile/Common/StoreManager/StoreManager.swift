//
//  StoreManager.swift
//  ShakeAndWin
//
//  Created by SKY on 14/12/20.
//

import Foundation
class StoreManager {
    static let shared = StoreManager()
    
    var gameId: String {
        get {
            return UserDefaults.standard.value(forKey: "gameId") as? String ?? ""
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: "gameId")
            UserDefaults.standard.synchronize()
        }
    }
    
    var token: String {
        get {
            return UserDefaults.standard.value(forKey: "token") as? String ?? ""
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: "token")
            UserDefaults.standard.synchronize()
        }
    }
    
    var msisdn: String {
        get {
            return UserDefaults.standard.value(forKey: "msisdn") as? String ?? ""
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: "msisdn")
            UserDefaults.standard.synchronize()
        }
    }
    
    @objc var language: String {
        get {
            return UserDefaults.standard.value(forKey: "language") as? String ?? ""
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: "language")
            UserDefaults.standard.synchronize()
        }
    }
    
    @objc var gameUrl: String {
        get {
            return UserDefaults.standard.value(forKey: "gameUrl") as? String ?? ""
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: "gameUrl")
            UserDefaults.standard.synchronize()
        }
    }
    
    var nationalId: String {
            get {
                return UserDefaults.standard.value(forKey: "nationalId") as? String ?? ""
            }
            set(value) {
                UserDefaults.standard.set(value, forKey: "nationalId")
                UserDefaults.standard.synchronize()
            }
        }
        
        var nationalIdType: String {
            get {
                return UserDefaults.standard.value(forKey: "nationalIdType") as? String ?? ""
            }
            set(value) {
                UserDefaults.standard.set(value, forKey: "nationalIdType")
                UserDefaults.standard.synchronize()
            }
        }
        
        var deviceToken: String {
            get {
                return UserDefaults.standard.value(forKey: "deviceToken") as? String ?? ""
            }
            set(value) {
                UserDefaults.standard.set(value, forKey: "deviceToken")
                UserDefaults.standard.synchronize()
            }
        }


    func removeMsisdn() {
            UserDefaults.standard.removeObject(forKey: "msisdn")
            UserDefaults.standard.synchronize()
        }
    
    func removeGameId() {
        UserDefaults.standard.removeObject(forKey: "gameId")
        UserDefaults.standard.synchronize()
    }
        func removeNationalId() {
            UserDefaults.standard.removeObject(forKey: "nationalId")
            UserDefaults.standard.synchronize()
        }
}
