//
//  UIImageView.swift
//  Gamification
//
//  Created by SKY on 15/05/21.
//

import UIKit

    var imageCache = NSCache<AnyObject, AnyObject>()

    extension UIImageView {
        func downloadImage(url: String,placeHolder: UIImage? = nil) {
            guard let url1 = URL(string: url) else { self.image = UIImage(named: "Loading", in: Bundle(for: Self.self), compatibleWith: nil)
                return }
            
            self.image = placeHolder//UIImage(named: "Loading", in: Bundle(for: Self.self), compatibleWith: nil)
            
            if let cacheImage = imageCache.object(forKey: url as AnyObject) as? UIImage {
                self.image = cacheImage
                return
            }
            
            print("url is \(url)")
            var req = URLRequest(url: url1)
            req.httpMethod = "GET"
            req.timeoutInterval = 30
            let accessToken = StoreManager.shared.token
            req.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            //req.addValue(accessToken, forHTTPHeaderField: "security_token")
            req.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let task = URLSession.shared.dataTask(with: req) { (data, responce, error) in
                if let data = data {
                    
                    
                    
                    let image = UIImage(data: data)
                    
                    if let img = image {
                    imageCache.setObject(img, forKey: url as AnyObject)
                    } else {
                        let imgData = UIImage(named: "oops")
                        imageCache.setObject(imgData!, forKey: url as AnyObject)
                    }
                    DispatchQueue.main.async {
                        self.image = image ?? UIImage(named: "Loading", in: Bundle(for: GuessBrandView.self), compatibleWith: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        let imgData = UIImage(named: "Loading")?.pngData()
                        self.image = UIImage(data: imgData!)
                    }
                }
            }
            task.resume()
        }
}
