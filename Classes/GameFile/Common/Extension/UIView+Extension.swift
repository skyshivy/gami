//
//  UIView+Extension.swift
//  Gamification
//
//  Created by SKY on 15/05/21.
//

import UIKit

extension UIView {
    func addBorder(width: CGFloat, redius: CGFloat? = nil,opacity: CGFloat = 1, color: UIColor = .clear) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.withAlphaComponent(opacity).cgColor
        self.layer.cornerRadius = self.frame.height/(redius ?? 2)
    }
    
    func roundCorners(corners: UIRectCorner,bound:CGRect, radius: CGFloat) {
            let path = UIBezierPath(roundedRect: bound, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    
    func animateViewScale(containerView: UIView?) {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        containerView?.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(withDuration: 0.3, animations: {
            containerView?.transform = .identity
            self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }) { (done) in
            
        }
    }
    
    func animateViewScaleRemove(containerView: UIView?, complition: (()->Void)?) {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        containerView?.transform = .identity//CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.3, animations: {
            containerView?.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
            self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        }) { (finished) in
            self.removeFromSuperview()
            complition?()
        }
    }
    
    func animateBackground(containerView: UIView?) {
        
        self.transform = CGAffineTransform()
        self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        containerView?.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
        UIView.animate(withDuration: 0.3, animations: {
            containerView?.transform = .identity
            self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        }) { (done) in
            
        }
    }
    
    func animateAndRemove(containerView: UIView?, complition: (()->Void)?) {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        UIView.animate(withDuration: 0.3, animations: {
            containerView?.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height)
            self.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        }) { (finished) in
            self.removeFromSuperview()
            complition?()
        }
    }
}
