//
//  Common.swift
//  Gamification
//
//  Created by SKY on 30/12/20.
//  Copyright © 2020 SIXDEE. All rights reserved.
//

import Foundation
class Common {
    static func getRandonNo() -> String {
        return String("\(Int.random(in: 11111111..<99999999))")
    }
    static func getTimeStamp()-> String {
        let d = Date()
        let df = DateFormatter()
        df.dateFormat = "YYMMDDHHmmss"
        let timeStamp = df.string(from: d)
        return "\(timeStamp)"
    }
}
