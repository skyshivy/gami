//
//  Error.swift
//  Gamification
//
//  Created by SKY on 30/12/20.
//  Copyright © 2020 SIXDEE. All rights reserved.
//

import Foundation
struct ErrorData {
    var message: String?
    var imageUrl: String?
    var errorFor: ResultFor? = .shakeAndWin
    var warning: String?
    var description: String?
}
