//
//  GuestWin.swift
//  Gamification
//
//  Created by SKY on 06/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import Foundation

class GuestWin: Decodable {
    var questionDetails: QuestionDetails?
    var questionaire: [Questionaire]?
    var respCode: String?
    var respDesc: String?
    var id: String?
    var numberOfQuestions: Int?
    var questionType: String?
    var timeBound: Bool?
    var timeToComplete: Int?
    //let answered: Int? = nil
}

class QuestionDetails: Decodable {
    let questionId: String
    let questionType: String
    let timeBound: Bool
    let timeToComplete: Int
    let numberOfQuestions: Int
    let questionaire: [Questionaire]
}

class Questionaire: Decodable {
    let questionId: String
    let question: String
    let image: String
    let options: [Option]
    var answered: Bool? = nil
    var optionId: String? = ""
    let questionType: String
}

class Option: Decodable{
    let option: String
    let correctAnswer: Bool?
    
    let rightOption: Bool?
}

class GuestAnswer: Decodable {
    let achievement: [Achievment]
}

class Achievment: Decodable {
    let achievmentId: String
    let achievementType: String
    let expiryDate: String
    let displayDetails: [DisplayDetails]
}
