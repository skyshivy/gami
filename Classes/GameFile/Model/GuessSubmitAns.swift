//
//  GuessSubmitAns.swift
//  Gamification
//
//  Created by SKY on 12/03/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import Foundation
class GuessSubmitAns: Decodable {
    var requestId: String?
    var timestamp: String?
    var respCode: String?
    var respDesc: String?
    var code: String?
    var message: String?
    var reason: String?
    var status: String?
    var responseObject: [GuessSubmitAnsResp]?
}

class GuessSubmitAnsResp: Decodable {
    var displayDetails:[DisplayDetails]
}
