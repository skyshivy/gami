//
//  ShakeBoxViewHelper.swift
//  Gamification
//
//  Created by SKY on 03/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit
enum ShakeBoxViewType {
    case jump
    case shake
    case open
}
class ShakeBoxViewHelper {
    var view: ShakeBoxView?
    //var rotateAngle = CGFloat(0.10906585)
    func loadShakeBoxView(sourceView: UIView,type: ShakeBoxViewType, fast: Bool = false, complition:(()->Void)? = nil) {
        removeShakeEggView()
        self.view?.hideSpark()
        view = ShakeBoxView.loadXib()
        view?.boxOpened = complition
        view?.frame = sourceView.bounds
        sourceView.addSubview(view!)
        self.checkBoxAction(type: type)
    }
    
    func checkBoxAction(type: ShakeBoxViewType) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            switch type {
            case .jump:
                self.view?.jumpBox()
            case .shake:
                self.view?.shakeBox()
            case .open:
                self.view?.stopShakeBox(complition: {
                    self.view?.hideStarSpark()
                    self.view?.animateOpenBox()
                })
            }

        }
    }
    
    func removeShakeEggView() {
        view?.removeFromSuperview()
        view = nil
    }
}
