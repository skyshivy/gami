//
//  ShakeBoxView.swift
//  Gamification
//
//  Created by SKY on 03/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit

class ShakeBoxView: UIView {
    @IBOutlet weak var sparkLeftImageView: UIImageView!
    @IBOutlet weak var sparkRightImageView: UIImageView!
    @IBOutlet weak var leftEyeImageView: UIImageView!
    @IBOutlet weak var rightEyeImageView: UIImageView!
    @IBOutlet weak var mouthImageView: UIImageView!
    @IBOutlet weak var boxCapImageView: UIImageView!
    @IBOutlet weak var boxImageView: UIImageView!
    @IBOutlet weak var topYellowImageView: UIImageView!
    @IBOutlet weak var topWhiteImageView: UIImageView!
    @IBOutlet weak var rightYImageView: UIImageView!
    @IBOutlet weak var leftWImageView: UIImageView!
    @IBOutlet weak var leftYImageView: UIImageView!
    var stopAnim: Bool = false
    var rotateAngle = CGFloat(0.30906585)
    //var rightRotateAngle = CGFloat(0.30906585)//CGFloat(0.050906585)
    var animationTime:TimeInterval = 0.30
    let downJumpX: CGFloat = 20
    
    let downCapJumpX: CGFloat = 35
    let scale: CGFloat = 0.8
    var boxOpened: (()->Void)?
    static func loadXib() -> ShakeBoxView {
        return UINib(nibName: "ShakeBoxView", bundle:Bundle(for: ShakeBoxView.self)).instantiate(withOwner: self, options: nil).first as! ShakeBoxView
    }

    func stopShakeBox(complition:(()->Void)? = nil) {
        DispatchQueue.main.async {
            self.hideStarSpark()
        }
        UIView.animate(withDuration: animationTime) {
            self.leftEyeImageView.transform = .identity
            self.rightEyeImageView.transform = .identity
            self.mouthImageView.transform = .identity
            self.topWhiteImageView.transform = .identity
            self.topYellowImageView.transform = .identity
            self.leftWImageView.transform = .identity
            self.leftYImageView.transform = .identity
            self.rightYImageView.transform = .identity
            self.boxCapImageView.transform = .identity
            self.boxImageView.transform = .identity
        } completion: { (done) in
            complition?()
        }
    }
    
    func jumpBox() {
        hideSpark()
        UIView.animate(withDuration: animationTime, delay: 0.0, options: .curveEaseInOut) {
            self.leftEyeImageView.transform = CGAffineTransform(translationX: 0, y: -self.downJumpX).scaledBy(x: 2, y: 1)
            self.rightEyeImageView.transform = CGAffineTransform(translationX: 0, y: -self.downJumpX).scaledBy(x: 2, y: 1)
            self.mouthImageView.transform = CGAffineTransform(translationX: 0, y: -self.downJumpX).rotated(by: -2*self.rotateAngle)
            self.topWhiteImageView.transform = CGAffineTransform(translationX: 0, y: -self.downCapJumpX).scaledBy(x: self.scale, y: self.scale).rotated(by: 5*self.rotateAngle)

            self.topYellowImageView.transform = CGAffineTransform(translationX: 0, y: -self.downCapJumpX).scaledBy(x: self.scale, y: self.scale).rotated(by: 5*self.rotateAngle)
            self.leftWImageView.transform = CGAffineTransform(translationX: 0, y: -self.downCapJumpX).scaledBy(x: self.scale, y: self.scale).rotated(by: 5*self.rotateAngle)
            
            self.leftYImageView.transform = CGAffineTransform(translationX: 0, y: -self.downJumpX).scaledBy(x: self.scale, y: self.scale).rotated(by: 5*self.rotateAngle)
            self.rightYImageView.transform = CGAffineTransform(translationX: 0, y: -self.downJumpX).scaledBy(x: self.scale, y: self.scale).rotated(by: 5*self.rotateAngle)
            self.boxCapImageView.transform = CGAffineTransform(translationX: 0, y: -self.downCapJumpX)
            self.boxImageView.transform = CGAffineTransform(translationX: 0, y: -self.downJumpX)
        } completion: { (done) in
            UIView.animate(withDuration: self.animationTime) {
                self.leftEyeImageView.transform = .identity
                self.rightEyeImageView.transform = .identity
                self.mouthImageView.transform = .identity
                self.topWhiteImageView.transform = .identity
                self.topYellowImageView.transform = .identity
                self.leftWImageView.transform = .identity
                self.leftYImageView.transform = .identity
                self.rightYImageView.transform = .identity
                self.boxCapImageView.transform = .identity
                self.boxImageView.transform = .identity
            }
            DispatchQueue.main.asyncAfter(deadline: .now()+1.3) {
                self.jumpBox()
            }
        }

    }
    
    func hideStarSpark() {
        self.topWhiteImageView.isHidden = true
        self.topYellowImageView.isHidden = true
        self.leftWImageView.isHidden = true
        self.leftYImageView.isHidden = true
        self.rightYImageView.isHidden = true
     
    }
    func hideSpark() {
        DispatchQueue.main.async {
        self.sparkLeftImageView.isHidden = true
        self.sparkRightImageView.isHidden = true
        }
    }
    
    func shakeBox() {
        hideSpark()
        hideStarSpark()
        UIView.animate(withDuration: animationTime, delay: 0.0, options: .curveEaseIn) {
            self.leftEyeImageView.transform = CGAffineTransform(translationX: -100, y: 0)
            //.scaledBy(x: 2, y: 1)
            
            self.rightEyeImageView.transform = CGAffineTransform(translationX: -100, y: 0)
            //.scaledBy(x: 2, y: 1)
            self.mouthImageView.transform = CGAffineTransform(translationX: -100, y: 0)
            //.rotated(by: -2*self.rotateAngle)
            self.boxCapImageView.transform = CGAffineTransform(translationX: -100, y: 0).rotated(by: self.rotateAngle)
            self.boxImageView.transform = CGAffineTransform(translationX: -100, y: 0)
                //.rotated(by: -self.rotateAngle)
        } completion: { (done) in
            UIView.animate(withDuration: self.animationTime) {
                self.leftEyeImageView.transform = CGAffineTransform(translationX: 100, y: 0)
                self.rightEyeImageView.transform = CGAffineTransform(translationX: 100, y: 0)
                self.mouthImageView.transform = CGAffineTransform(translationX: 100, y: 0)
                self.boxCapImageView.transform = CGAffineTransform(translationX: 100, y: 0).rotated(by: -self.rotateAngle)
                self.boxImageView.transform = CGAffineTransform(translationX: 100, y: 0)
                    //.rotated(by: self.rightRotateAngle)
            } completion: { (done) in
                if self.stopAnim {
                    self.stopShakeBox(complition: self.animateOpenBox)
                } else {
                    self.shakeBox()
                }
            }
        }
    }
    
    func animateOpenBox() {
        print("stopped")
        let path = UIBezierPath()

            let imageSun = UIImage(named: "CapVer", in: Bundle(for: Self.self), compatibleWith: nil)
        boxCapImageView.image = imageSun
        let imgCenterY = boxCapImageView.center.y
        let imgCenterX = boxCapImageView.center.x
        path.move(to: CGPoint(x: imgCenterX,y: imgCenterY))
        path.addLine(to: CGPoint(x: imgCenterX-30, y: imgCenterY-20))
        path.addLine(to: CGPoint(x: imgCenterX-50, y: imgCenterY-30))
        path.addLine(to: CGPoint(x: imgCenterX-80, y: imgCenterY-30))
        path.addLine(to: CGPoint(x: imgCenterX-90, y: imgCenterY-20))
        path.addLine(to: CGPoint(x: imgCenterX-100, y: imgCenterY-10))
        path.addLine(to: CGPoint(x: imgCenterX-100, y: imgCenterY-0))
        path.addLine(to: CGPoint(x: imgCenterX-100, y: imgCenterY+10))
        path.addLine(to: CGPoint(x: imgCenterX-100, y: imgCenterY+30))
        path.addLine(to: CGPoint(x: imgCenterX-100, y: imgCenterY+50))
        path.addLine(to: CGPoint(x: imgCenterX-100, y: imgCenterY+70))
        path.addLine(to: CGPoint(x: imgCenterX-100, y: imgCenterY+80))
        
        boxCapImageView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        let animation = CAKeyframeAnimation(keyPath: "position")
        animation.path = path.cgPath
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        animation.repeatCount = 0
        animation.duration = 0.20
        boxCapImageView.layer.add(animation, forKey: "animate position along path")
        self.sparkLeftImageView.isHidden = false
        self.sparkRightImageView.isHidden = false
        self.sparkRightImageView.transform = CGAffineTransform(translationX: 0, y: 100).scaledBy(x: 0.0, y: 0.0)
        self.sparkLeftImageView.transform = CGAffineTransform(translationX: 0, y: 100).scaledBy(x: 0.0, y: 0.0)
        UIView.animate(withDuration: animationTime) {
            self.sparkRightImageView.transform = .identity
            self.sparkLeftImageView.transform = .identity
        } completion: { (done) in
            self.boxOpened?()
        }
    }
}
