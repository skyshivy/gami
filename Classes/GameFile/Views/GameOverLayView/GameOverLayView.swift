//
//  GameOverLayView.swift
//  ShakeAndWin
//
//  Created by SKY on 04/12/20.
//

import UIKit
enum GameOverLayViewAction {
    case gotIt
}
class GameOverLayView: UIView {
    @IBOutlet weak var shakeLabel: UILabel!
    @IBOutlet weak var gotItButton: UIButton!
    var gameOverLayViewHandler: ((GameOverLayViewAction)->Void)?
    
    static func loadXib() -> GameOverLayView {
        return UINib(nibName: "GameOverLayView", bundle:Bundle(for: GameOverLayView.self)).instantiate(withOwner: self, options: nil).first as! GameOverLayView
    }
    
    @IBAction func gotItButtonAction() {
        gameOverLayViewHandler?(.gotIt)
    }
}
