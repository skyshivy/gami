//
//  GameOverLayViewHelper.swift
//  ShakeAndWin
//
//  Created by SKY on 04/12/20.
//

import UIKit
class GameOverLayViewHelper {
    
    var view: GameOverLayView?
    var isShakeBox: Bool = false
    func loadGameOverLayView(sourceView: UIView, complition: ((GameOverLayViewAction)->Void)?) {
        removeGameOverLayViewView()
        view = GameOverLayView.loadXib()
        view?.frame.size = CGSize(width: UIScreen.main.bounds.width - 50, height: 300)//sourceView.bounds
        view?.center = sourceView.center
        sourceView.addSubview(view!)
        view?.gameOverLayViewHandler = complition
        view?.isHidden = false
        localize()
    }
    
    func localize() {
        if isShakeBox {
            view?.shakeLabel.text = "Shake your phone to open giftbox".localizeFor(Self.self)
            view?.gotItButton.setTitle("Got it".localizeFor(Self.self), for: .normal)
        } else {
            view?.shakeLabel.text = "Shake your phone to crack this egg".localizeFor(Self.self)
            view?.gotItButton.setTitle("Got it".localizeFor(Self.self), for: .normal)
        }
    }
    func removeGameOverLayViewView() {
        view?.removeFromSuperview()
        view = nil
    }
}
