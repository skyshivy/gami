//
//  TermsAndCondition.swift
//  ShakeAndWin
//
//  Created by SKY on 04/12/20.
//

import UIKit
enum TermsAndConditionAction {
    case close
    case agree
    case dismissed
    case ok(Bool)
}
class TermsAndCondition: UIView {
    @IBOutlet weak var termsAndConLabel: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var checkBoxLabel: UILabel!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewConstraints: NSLayoutConstraint!
    @IBOutlet weak var buttonContainerStackView: UIStackView!
    @IBOutlet weak var guessViewStackView: UIStackView!
    var termsAndConditionActionHandler: ((TermsAndConditionAction)->Void)?
    static func loadXib() -> TermsAndCondition {
        return UINib(nibName: "TermsAndCondition", bundle: Bundle(for: TermsAndCondition.self)).instantiate(withOwner: self, options: nil).first as! TermsAndCondition
    }
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func populateView(type: TermsAndCondType) {
        switch type {
        case .guessBrand:
            termsForGuessView()
        default:
            localize()
            termsForAll()
        }
    }
    
    func termsForAll() {
        closeButton.isHidden = false
        buttonContainerStackView.isHidden = false
        guessViewStackView.isHidden = true
        containerView.roundCorners(corners: [.topLeft,.topRight], bound: self.bounds, radius: 0)
    }
    func termsForGuessView() {
        containerView.roundCorners(corners: [.topLeft,.topRight], bound: self.bounds, radius: 10)
        closeButton.isHidden = true
        buttonContainerStackView.isHidden = true
        guessViewStackView.isHidden = false
        termsAndConLabel.text = "Terms".localizeFor(Self.self)
        okButton.setTitle("Ok".localizeFor(Self.self), for: .normal)
    }
    
    func localize() {
        discriptionLabel.text = "By tapping agree button below, you are agreeing to the above Terms & Conditions.".localizeFor(Self.self)
        termsAndConLabel.text = "Terms And Conditions".localizeFor(Self.self)
        agreeButton.setTitle("Ok".localizeFor(Self.self), for: .normal)
        textView.textAlignment = Utility.isRTL() ? .right : .left
    }
    
    @IBAction func closeButtonAction() {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {[weak self] in
            self?.termsAndConditionActionHandler?(.dismissed)
        }
        termsAndConditionActionHandler?(.close)
    }
    
    @IBAction func agreeButtonAction() {
        termsAndConditionActionHandler?(.agree)
    }
    
    @IBAction func checkBoxButtonAction() {
        self.checkBoxButton.isSelected = !self.checkBoxButton.isSelected
        //termsAndConditionActionHandler?(.agree)
    }
    @IBAction func okButtonAction() {
        termsAndConditionActionHandler?(.ok(self.checkBoxButton.isSelected))
        termsAndConditionActionHandler?(.agree)
    }
}
