//
//  GuessBrandView.swift
//  Gamification
//
//  Created by SKY on 05/01/21.
//  Copyright © 2021 SIXDEE. All rights reserved.
//

import UIKit
enum GuessBrandViewAction {
    case timeOver(Int,GuestWin)
    case gameOver(Int,GuestWin)
}
class GuessBrandView: UIView {
    @IBOutlet weak var firstButton: CustomButtonView!
    @IBOutlet weak var secondButton: CustomButtonView!
    @IBOutlet weak var thirdButton: CustomButtonView!
    @IBOutlet weak var fourthButton: CustomButtonView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var brandImageView: UIImageView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var countDownlLabel: UILabel!
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var buttonContainerStackView: UIStackView!
    var guestBrandHandler: ((GuessBrandViewAction)->Void)?
    var info: GuestWin?
    var index: Int = 0
    var totalQuestion: Int = 0
    var noOfOptions: Int = 0
    var countDown: Int = 0
    var timer: Timer?
    var utilizedTime: Int = 0
    static func loadXib() -> GuessBrandView {
        return UINib(nibName: "GuessBrandView", bundle: Bundle(for: Self.self)).instantiate(withOwner: self, options: nil).first as! GuessBrandView
    }
    
   private func initialSetup() {
        self.pageControl.isUserInteractionEnabled = false
        self.pageControl.isHidden = true
    }
    
    func populateView(info: GuestWin) {
        print("populateView \(info)")
        if (info.questionaire?.count ?? 0) <= 0 {return}
        noOfOptions = info.questionaire?[index].options.count ?? 0
        pageControl.numberOfPages = info.questionaire?.count ?? 0
        totalQuestion = info.questionaire?.count ?? 0
        DispatchQueue.main.async {
            self.pageControl.isHidden = ((info.questionaire?.count ?? 0) <= 1) ? true : false//false
        }
        self.info = info
        initialSetup()
        buttonHandler()
        updateView(info: info)
        startTimer()
        guard let time = self.info?.timeToComplete else {return}
        countDown = time
    }
    
    func startTimer() {
        stopTimer()
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        }
    }
    
    func startCountDown() {
        countDown -= 1
        if countDown <= 0 {
            self.guestBrandHandler?(.gameOver(utilizedTime, info!))
            stopTimer()
        }
        self.countDownlLabel.text = "\(countDown)"
        let str = "\(countDown)"
        self.countDownlLabel.text = str.convertIntIntoHMS()
    }
    
    @objc func updateTime() {
        utilizedTime += 1
        startCountDown()
    }
    
    func stopTimer(){
        utilizedTime = 0
        timer?.invalidate()
        timer = nil
    }

    
    func updateView(info: GuestWin) {
        pageControl.currentPage = index
        noOfOptions = info.questionaire?[index].options.count ?? 0
        populateButtonAndLabels(info: info)
        displayButtonBasedOnAnswer(info: info)
        displayQuestionImage(info: info)
    }
    
    func buttonHandler() {
        firstButton.handle = firstButtonHandler
        secondButton.handle = secondButtonHandler
        thirdButton.handle = thirdButtonHandler
        fourthButton.handle = fourthButtonHandler
    }
    
    func populateButtonAndLabels(info: GuestWin) {
        questionLabel.text = info.questionaire?[index].question
    }
    
    func displayQuestionImage(info: GuestWin) {
        var imageUr = info.questionaire?[index].image ?? ""
        var finalUrl = ""

        if imageUr.contains("http") {
           finalUrl = imageUr
        } else {
            finalUrl = Constant.imageUrl+"\(info.questionaire?[index].image ?? "")"
        }
        brandImageView.downloadImage(url: finalUrl,placeHolder: UIImage(named: "Loading", in: Bundle(for: GuessBrandView.self), compatibleWith: nil))
    }
    
    func displayButtonBasedOnAnswer(info: GuestWin) {
        
        firstButton.isHidden = true
        secondButton.isHidden = true
        thirdButton.isHidden = true
        fourthButton.isHidden = true
        let detail = info.questionaire
        if noOfOptions  == 1 {
            self.firstButton.isHidden = false
            self.firstButton.answerButton.setTitle(detail?[index].options[0].option, for: .normal)
        } else if noOfOptions == 2 {
            self.firstButton.isHidden = false
            self.secondButton.isHidden = false
            self.firstButton.answerButton.setTitle(detail?[index].options[0].option, for: .normal)
            self.secondButton.answerButton.setTitle(detail?[index].options[1].option, for: .normal)
        } else if noOfOptions == 3 {
            self.firstButton.isHidden = false
            self.secondButton.isHidden = false
            self.thirdButton.isHidden = false
            self.firstButton.answerButton.setTitle(detail?[index].options[0].option, for: .normal)
            self.secondButton.answerButton.setTitle(detail?[index].options[1].option, for: .normal)
            self.thirdButton.answerButton.setTitle(detail?[index].options[2].option, for: .normal)
        } else if noOfOptions == 4 {
            self.firstButton.isHidden = false
            self.secondButton.isHidden = false
            self.thirdButton.isHidden = false
            self.fourthButton.isHidden = false
            self.firstButton.answerButton.setTitle(detail?[index].options[0].option, for: .normal)
            self.secondButton.answerButton.setTitle(detail?[index].options[1].option, for: .normal)
            self.thirdButton.answerButton.setTitle(detail?[index].options[2].option, for: .normal)
            self.fourthButton.answerButton.setTitle(detail?[index].options[2].option, for: .normal)
        } else {
            print("Hide all Buttons")
        }
    }
    
    func setBrandImage(index: Int) {
        let url = Constant.imageUrl+"\(self.info?.questionaire?[index].image ?? "opps")"
        brandImageView.downloadImage(url: url)
        print("Image url is \(url)")
    }
    
    func userInteractionOfButton(enable: Bool) {
        firstButton.answerButton.isUserInteractionEnabled = enable
        secondButton.answerButton.isUserInteractionEnabled = enable
        thirdButton.answerButton.isUserInteractionEnabled = enable
        fourthButton.answerButton.isUserInteractionEnabled = enable
    }
    
    func fadeAway() {
        UIView.animate(withDuration: 0.2) {
            self.imageContainerView.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
            self.questionLabel.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
            self.firstButton.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
            self.secondButton.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
            self.thirdButton.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
            self.fourthButton.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0)
        } completion: { (done) in
            self.fadeIn()
        }
    }
    
    func fadeIn() {
        self.imageContainerView.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        self.questionLabel.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        self.firstButton.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        self.secondButton.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        self.thirdButton.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        self.fourthButton.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        UIView.animate(withDuration: 0.2) {
            self.imageContainerView.transform = .identity
            self.questionLabel.transform = .identity
            self.firstButton.transform = .identity
            self.secondButton.transform = .identity
            self.thirdButton.transform = .identity
            self.fourthButton.transform = .identity
        } completion: { (done) in
            print("")
        }
    }
    
    func resetButton() {
        firstButton.resetButton(title: "")
        secondButton.resetButton(title: "")
        thirdButton.resetButton(title: "")
        fourthButton.resetButton(title: "")
    }
    
    func checkCorrectAnswer(buttonIndex: Int) -> Bool {
        let isCorrect = self.info?.questionaire?[index].options[buttonIndex].correctAnswer ?? false
        self.info?.questionaire?[index].answered = isCorrect
        self.info?.questionaire?[index].optionId = self.info?.questionaire?[index].options[buttonIndex].option ?? ""
        return isCorrect
    }
    
    func answeredAllQuestion() {
        print("Answered all questions")
        if let data = self.info {
            self.guestBrandHandler?(.gameOver(utilizedTime, data))
        }
        self.stopTimer()
    }
    
    /*
    
    func apiCallForAnswer() {
        var timeUtilized: Int = 10
        var correctAnswer: String = "10"
        let url = "http://49.206.240.154:8150/Gamification-1.0/Gamification/executeGame"
        var req = URLRequest(url: URL(string: url)!)
        req.setValue("2", forHTTPHeaderField: "game")
        req.setValue("3", forHTTPHeaderField: "milestone")
        req.setValue(Common.getRandonNo(), forHTTPHeaderField: "requestId")
        req.setValue(StoreManager.shared.msisdn, forHTTPHeaderField: "Msisdn")
        req.setValue(StoreManager.shared.language, forHTTPHeaderField: "Language")
        req.setValue("submitAnswers", forHTTPHeaderField: "operationId")
        
        let myDict: Dictionary = [
            "RecordQASubmission": ["questionId": self.info?.questionDetails.questionId ?? "4","timeUtilized": timeUtilized,"correctAnswers": correctAnswer, "totalQuestions": self.info?.questionDetails.numberOfQuestions ?? 2]
        ] as [String : Any]
        
        
        NetworkManager.postWithHeader(myStruct: GuestAnswer.self, urlReq: req, request: myDict) { (data, error) in
            print("Data postWithHeader is \(data) ")
            print("Data postWithHeader is \(error) ")
        }
    }
    */
}
extension GuessBrandView {
    func firstButtonHandler() {
        checkCorrectAnswer(buttonIndex: 0) ? firstButton.rightButtonSelected() : firstButton.wrongButtonSelected()
        commonMethod()
        print("first")
    }
    func secondButtonHandler() {
        checkCorrectAnswer(buttonIndex: 1) ? secondButton.rightButtonSelected() : secondButton.wrongButtonSelected()
        commonMethod()
        print("Second")
    }
    func thirdButtonHandler() {
        checkCorrectAnswer(buttonIndex: 2) ? thirdButton.rightButtonSelected() : thirdButton.wrongButtonSelected()
        commonMethod()
        print("Third")
    }
    func fourthButtonHandler() {
        
        commonMethod()
        print("Fourth")
    }
    
    func commonMethod() {
        index += 1
        userInteractionOfButton(enable: false)
        if index>=totalQuestion {answeredAllQuestion();return}
        DispatchQueue.main.asyncAfter(deadline: .now()+0.6) {
            if let data = self.info {
                self.resetButton()
                self.updateView(info: data)
                self.userInteractionOfButton(enable: true)
                self.fadeAway()
            }
        }
    }
    
}
