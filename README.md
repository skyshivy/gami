# Gami

[![CI Status](https://img.shields.io/travis/shivysky/Gami.svg?style=flat)](https://travis-ci.org/shivysky/Gami)
[![Version](https://img.shields.io/cocoapods/v/Gami.svg?style=flat)](https://cocoapods.org/pods/Gami)
[![License](https://img.shields.io/cocoapods/l/Gami.svg?style=flat)](https://cocoapods.org/pods/Gami)
[![Platform](https://img.shields.io/cocoapods/p/Gami.svg?style=flat)](https://cocoapods.org/pods/Gami)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Gami is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Gami'
```

## Author

shivysky, shiv.yadav@6dtech.co.in

## License

Gami is available under the MIT license. See the LICENSE file for more info.
