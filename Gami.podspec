#
# Be sure to run `pod lib lint Gami.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'Gami'
s.version          = '0.1.0'
s.summary          = 'This pod will open game file'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
'This pod will open game file need to import and addi it'
DESC

s.homepage         = 'https://github.com/shivysky/Gami'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'shivysky' => 'shiv.yadav@6dtech.co.in' }
s.source           = { :git => 'https://github.com/shivysky/Gami.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '12.0'

s.source_files = 'Classes/**/*'
s.swift_version = '5.0'
s.platforms = {
"ios":"12.0"
}
# s.resource_bundles = {
#   'Gami' => ['Gami/Assets/*.png']
# }

# s.public_header_files = 'Pod/Classes/GameFile/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'AFNetworking', '~> 2.3'
end
